# TP2 : Serveur Web
## O. Prérequis

Faites en sorte que les machines utilisent des noms de domaine

* Initialisation/modification des adresses IP des machines clonés afin de s'y connecter en SSH (192.168.15.1X  ---> .10 à .12)
```bash
$ sudo nano /etc/sysconfig/network-scripts/ifcfg-ens33

#Adresse IP de CentOS7_LINUX2
IPADDR=192.168.15.11

$ systemctl restart network
```


* Modification de leur fichier /etc/hosts : Ajout des lignes suivantes dans le fichier hosts des 3 VM

```bash
192.168.15.10 web.tp2.cesi
192.168.15.11 db.tp2.cesi
192.168.15.12 rp.tp2.cesi
```


* Modification de leur fichier /etc/hostname, les machines seront nommées web.tp2.cesi, db.tp2.cesi et rp.tp2.cesi
```bash
--- db.tp2.cesi ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2014ms
rtt min/avg/max/mdev = 0.019/0.031/0.038/0.008 ms
[Tsacoco@localhost network-scripts]$ ping web.tp2.cesi
PING web.tp2.cesi (192.168.15.10) 56(84) bytes of data.
64 bytes from web.tp2.cesi (192.168.15.10): icmp_seq=1 ttl=64 time=0.424 ms

--- web.tp2.cesi ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1005ms
rtt min/avg/max/mdev = 0.424/0.448/0.472/0.024 ms
[Tsacoco@localhost network-scripts]$ ping rp.tp2.cesi
PING rp.tp2.cesi (192.168.15.12) 56(84) bytes of data.
64 bytes from rp.tp2.cesi (192.168.15.12): icmp_seq=1 ttl=64 time=0.425 ms

--- rp.tp2.cesi ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1009ms
rtt min/avg/max/mdev = 0.425/0.451/0.477/0.026 ms

```


## I. Base de données


* Installer le paquet mariadb-server, puis démarrer le service associé.

Sur db.tp2.cesi : 
```bash
$ sudo yum install mariadb-server

$ sudo systemctl start mariadb.service
```


* Se connecter à la base
```bash
$ mysql -u root -p

Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 6
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> Ctrl-C -- exit!
```

* Modification du mot de passe root
```bash
$ mysql -u root -p

SET PASSWORD FOR 'root'@'localhost' = PASSWORD('XXXXX');
FLUSH PRIVILEGES;
```
* Créer une base
```bash
$ mysql -u root -p

MariaDB [(none)]> create database Mendori;

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| Mendori            |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)
```
* Créer un utilisateur
```bash
CREATE USER 'tsacoco'@'localhost' IDENTIFIED BY 'XXXXXX';
```
L'utilisateur est visible dans la  base de donnée mysql 
```bash
MariaDB [(none)]> use mysql

MariaDB [mysql]> select User from user where User = "Tsacoco";
+---------+
| User    |
+---------+
| Tsacoco |
+---------+
1 row in set (0.00 sec)
```
* Attribuer les droits sur la base de données à l'utilisateur
```bash
GRANT ALL PRIVILEGES ON * . * TO 'Tsacoco'@'localhost';
GRANT ALL on *.* TO Tsacoco@'web.tp2.cesi' IDENTIFIED BY 'XXXX';
GRANT ALL on *.* TO Tsacoco@'rp.tp2.cesi' IDENTIFIED BY 'XXXX';
```
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base
```bash
$ sudo ss -alnpt
```
Le port par défaut permettant d'accéder à la base est 3306. 
Ajout des sources 192.168.15.10 (web.tp2.cesi) et 192.168.15.12 (rp.tp2.cesi) dans la zone autorisée puis du port 3306/tcp
```bash
firewall-cmd --permanent --zone=trusted --add-source=192.168.15.10/32
firewall-cmd --permanent --zone=trusted --add-source=192.168.15.12/32
firewall-cmd --permanent --zone=trusted --add-port=3306/tcp
firewall-cmd  --reload
```

* Installation de mariadb depuis web.tp2.cesi et rp.tp2.cesi

```bash
$ sudo yum groupinstall -y mariadb
$ sudo yum groupinstall -y mariadb-client
```
* Vérification d'accès à la base depuis web.tp2.cesi 
```bash
$ mysql -h db.tp2.cesi -u Tsacoco -p

Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

## II. Serveur Web

* Installer un serveur Apache (paquet httpd dans CentOS).
Depuis web.tp2.cesi :
```bash
$ sudo yum install httpd
```
* Télécharger Wordpress : https://wordpress.org/latest.tar.gz

```bash 
$ sudo yum install wget

$ sudo wget https://wordpress.org/latest.tar.gz

$ systemctl start httpd.service

$ systemctl enable httpd.service
```

* Extraire l'archive wordpress dans un dossier qui pourra être servi par Apache

```bash
$ sudo mkdir /var/www/html/Mendori
$ sudo tar -xzvf /home/Tsacoco/latest.tar.gz -C /var/www/html/Mendori
```

* Renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données (dans son fichier de configuration)

```bash
$ sudo nano /var/www/html/wp-config.php 
```
Modification des lignes suivantes : 

```bash
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Mendori' );

/** MySQL database username */
define( 'DB_USER', 'Tsacoco' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Passe10?' );

/** MySQL hostname */
define( 'DB_HOST', 'db.tp2.cesi' );
```

* Accéder à l'interface de Wordpress afin de valider la bonne installation de la solution

http://192.168.15.10/wp-admin/index.php est accessible et il est possible de naviguer sur celle-ci.



## III. Reverse Proxy


Sur rp.tp2.cesi :


* Installer un serveur NGINX (paquet epel-release puis nginx sur CentOS)

```bash
$ sudo yum install epel-release
$ sudo yum install nginx
```
* Configurer NGINX pour qu'il puisse renvoyer vers le serveur web lorsqu'on l'interroge sur le port 80


```bash
$ nano /etc/nginx/nginx.conf

#Ajout/modification des lignes suivantes
#Dans  server {}
listen       80;
server_name  web.tp2.cesi;
#Dans location / {}
proxy_pass http://192.168.15.10;
```

* Configurer le firewall de NGINX afin d'accepter ce trafic

Sur rp.tp2.cesi : 
```bash
$ sudo firewall-cmd --permanent --zone=public --add-service=http
$ sudo firewall-cmd --permanent --zone=public --add-service=https
$ sudo firewall-cmd --reload
```

## IV. Un peu de sécu


### 1. fail2ban

* Installer fail2ban
```bash
$ sudo yum install epel-release
$ sudo yum install fail2ban
$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```
* Paramétrer fail2ban 
```bash
$ sudo nano /etc/fail2ban/jail.local

#Paramétrage de base
[DEFAULT]
bantime = 3600

findtime = 600
maxretry = 3
action = %(action_mwl)s

[sshd]
enabled = true
```

* Lancement et activation du service fail2ban
```bash
$ sudo systemctl start fail2ban.service
$ sudo systemctl enable fail2ban.service
```
* Avoir des informations sur les jails 
```bash
$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```
* Avoir des informations sur le jail sshd
```bash
$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     0
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     0
   `- Banned IP list:
```

### 2. HTTPS

#### A. Génération du certificat et de la clé privée associé

* Commande pour générer une clé et un certificat auto-signé depuis le serveur rp.tp2.cesi 
```bash
$ sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.tp2.cesi.crt

Common Name []: web.tp2.cesi
```
#### B. Configurer le reverse proxy
* Configurer le reverse proxy pour écouter sur le port 443 (TCP)
```bash
$ sudo nano /etc/nginx/nginx.conf
#Ajout de la ligne dans server{}
listen    443;

$ sudo systemctl restart nginx

$ ss -alnpt
LISTEN     0      128                              *:80
LISTEN     0      128                              *:443
```

* Mise en place du certificat et rediction de http vers https
```bash
$ sudo nano /etc/nginx/nginx.conf

events {}

http {
   server {

        listen      443;
        server_name web.tp2.cesi;
        ssl on;
        ssl_certificate "/etc/pki/tls/certs/web.tp2.cesi.crt";
        ssl_certificate_key "/etc/pki/tls/private/web.cesi.key";
        location / {
            proxy_pass   http://10.55.55.11;
        }
    }
   server{

        listen 80 default_server;
        server_name web.tp2.cesi;
        return 301 https://$host$request_uri;
         }
}

$ sudo systemctl restart nginx

```

### 3. Monitoring

#### A. Installation de Netdata
```bash
$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

* Ouvrir le port firewall 19999/tcp pour accéder à l'interface de Netdata
```bash
$ sudo firewall-cmd --add-port=19999/tcp --permanent
$ sudo systemctl restart firewalld
```

#### B. Alerting

* Création d'un webhook sur discord : https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks
* Définir une configuration qui permet de recevoir une alerte Discord si le disque OU la RAM sont remplis à + de 75%

Création du fichier health_alarm_notify.conf et ajout de la configuration suivante dans celui-ci
```bash
$ sudo nano /etc/netdata/health_alarm_notify.conf

SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/789410210560213012/53LeSts9Z9dQIa4R2QASx8RbAhA4pl6j3Vby9lc6z2bqq5K_ME0IzSvMpJcH4obJ6sgJ"
DEFAULT_RECIPIENT_DISCORD="alarms"
```
* Nous reçevons bien une notification discord : 
##### netdata on web.tp2.cesi

web.tp2.cesi needs attention, ipv4.tcphandshake (tcp), 10s ipv4 tcp resets received = 87.8 tcp resets/s
##### 10s ipv4 tcp resets received = 87.8 tcp resets/s
average TCP RESETS this host is receiving, over the last 10 seconds (this can be an indication that a service this host needs, has crashed; clear notification for this alarm will not be sent)
##### ipv4.tcphandshake
tcp
