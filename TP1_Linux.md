# TP1 Linux
## 0. Préliminaires

Installer vim sur la machine afin de faciliter l'édition des fichiers.
Désactiver SELinux :

```bash
$ sudo yum install vim-enhanced -y

$ sudo setenforce 0
$ sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config 
```


## I. Utilisateurs

```bash
$ sudo useradd Tsacoco --shell /bin/bash
$ sudo groupadd admins

```
Ajout des lignes dans /etc/sudoers


```bash
## Allows people in group admins to run all commands
%admins	ALL=(ALL)	ALL
```
Ajout de l'utilisateur dans le groupe admins

```bash
$ sudo usermod -a -G Tsacoco admins
```

Déposer la clé dans le fichier /home/<USER>/.ssh/authorized_keys de la machine que l'on souhaite administrer 

```bash
cp id_rsa /.ssh/authorized_keys
```


## II. Configuration réseau

### 1. Nom de domaine

```bash
sudo hostname mondial
```

### 2. Serveur DNS

Dans /etc/resolv.conf ajout de la ligne 
```bash
nameserver 192.168.15.10
```

## III. Partitionnement
### 1. Préparation de la VM

* Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.
```bash
$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   20G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   19G  0 part
  ├─centos-root 253:0    0   17G  0 lvm  /
  └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    3G  0 disk
sr0              11:0    1  4,4G  0 rom



$ sudo pvcreate /dev/sdb
$ sudo pvcreate /dev/sdc
```

### 2. Partitionnement
* Agréger les deux disques en un seul volume group
```bash
$ sudo vgcreate data /dev/sdb
$ sudo vgextend data /dev/sdc

$ sudo vgs
  VG      PV  LV  SN Attr   VSize   VFree
  centos   1   2   0 wz--n- <19,00g    0
  data     2   0   0 wz--n-   5,99g 5,99g

```

* Créer 3 logical volumes de 1 Go chacun
```bash
$ sudo lvcreate -L 1G data -n data1
$ sudo lvcreate -L 1G data -n data2
$ sudo lvcreate -L 1G data -n data3

$ sudo lvs
  LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root  centos -wi-ao---- <17,00g                                               
  swap  centos -wi-ao----   2,00g                                               
  data1 data   -wi-a-----   1,00g                                               
  data2 data   -wi-a-----   1,00g                                               
  data3 data   -wi-a-----   1,00g  
```

* Formater ces partitions en ext4
```bash
$ sudo mkfs -t ext4 /dev/data/data1
$ sudo mkfs -t ext4 /dev/data/data2
$ sudo mkfs -t ext4 /dev/data/data3
```
* Monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3
```bash
$ sudo mkdir part1
$ sudo mkdir part2
$ sudo mkdir part3

$ sudo mount /dev/data/data1 /mnt/part1
$ sudo mount /dev/data/data2 /mnt/part2
$ sudo mount /dev/data/data3 /mnt/part3

$ df -h
Sys. de fichiers        Taille Utilisé Dispo Uti% Monté sur
devtmpfs                  475M       0  475M   0% /dev
tmpfs                     487M       0  487M   0% /dev/shm
tmpfs                     487M    7,8M  479M   2% /run
tmpfs                     487M       0  487M   0% /sys/fs/cgroup
/dev/mapper/centos-root    17G    1,5G   16G   9% /
/dev/sda1                1014M    168M  847M  17% /boot
tmpfs                      98M       0   98M   0% /run/user/1000
tmpfs                      98M       0   98M   0% /run/user/1001
/dev/mapper/data-data1    976M    2,6M  907M   1% /mnt/part1
/dev/mapper/data-data2    976M    2,6M  907M   1% /mnt/part2
/dev/mapper/data-data3    976M    2,6M  907M   1% /mnt/part3
```
* Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.
```bash
/dev/data/data1 /mnt/part1 ext4 defaults 0 0
/dev/data/data2 /mnt/part2 ext4 defaults 0 0
/dev/data/data3 /mnt/part3 ext4 defaults 0 0
```
```bash
$ sudo umount /mnt/part1
$ sudo mount -av

/mnt/part1                successfully mounted
/mnt/part2                déjà monté
/mnt/part3                déjà monté
```
## IV. Gestion de services

### 1. Interaction avec un service existant

* Assurez-vous que le service firewalld est démarré et se lance automatiquement au démarrage
```bash
$ systemctl is-active firewalld
active
```
### 2. Création de service

#### A. Unité simpliste

* Créer un fichier web.service dans le répertoire /etc/systemd/system et y déposer le contenu
```bash
sudo touch /etc/systemd/system/web.service


$ cat web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target
```

* Ouvrir le port 8888 de la machine
```bash
$ sudo firewall-cmd --add-port=8888/tcp --permanent
$ sudo firewall-cmd --reload

$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens37
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* Demander à systemd de relire les fichiers de configuration :
```bash
$ sudo systemctl daemon-reload
$ sudo systemctl start web
$ sudo systemctl enable web

$ sudo systemctl status web
   web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since mar. 2020-12-15 14:38:38 CET; 4s ago
 Main PID: 1924 (python2)
   CGroup: /system.slice/web.service
           └─1924 /bin/python2 -m SimpleHTTPServer 8888

```

#### B. Modification de l'unité

* Créer un utilisateur web.

```bash
$ useradd web
```

* Modifiez l'unité de service web.service créé précédemment en ajoutant les clauses

Toujours dans le répertoire /etc/systemd/system

```bash
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
User=web
WorkingDirectory=/srv

[Install]
WantedBy=multi-user.target
```

* Création d'un fichier de test dans /srv puis redémarrage du service
```bash
mkdir /srv/test/


$ sudo systemctl daemon-reload
```