# TP3 : DNS, Backup et Docker

## I. DNS

### 1. Présentation

* Créer une nouvelle machine qui portera ce service DNS.

web.tp2.cesi
192.168.15.10/24
Serveur Web


db.tp2.cesi
192.168.15.11/24
Serveur de bases de données


rp.tp2.cesi
192.168.15.12/24
Reverse proxy


ns1.tp2.cesi
192.168.15.13/24
Serveur DNS

### 2. Mise en place

* Installer le paquet epel-release si ce n'est pas déjà fait. Puis les paquets bind et bind-utils

Sur ns1.tp2.cesi :
```bash 
$ sudo yum install epel-release 
$ sudo yum install bind
$ sudo yum install bind-utils
```
* Configurer /etc/named.conf
```bash  
$ sudo nano /etc/named.conf

options {
        listen-on port 53 { 192.168.15.13; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { 192.168.15.0/24; };

        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        /* Path to ISC DLV key */
        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";


};

zone "tp2.cesi" IN {
        type master;
        file "/var/named/tp2.cesi.db";
        allow-update { none; };

};


zone "15.168.192.in-addr.arpa" IN {
        type master;
        file "/var/named/15.168.192.db";
        
};


include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```
* Créer et configurer /var/named/tp2.cesi.db
```bash 
$ sudo nano /var/named/tp2.cesi.db

$TTL 604802
@ IN SOA ns1.tp2.cesi. root.tp2.cesi. (
                                                1001 ;Serial
                                                3H ;Refresh
                                                15M ;Retry
                                                1W ;Expire
                                                1D ;Minimum TTL
                                                )
;Name Server Information
@ IN NS ns1.tp2.cesi.

ns1 IN A 192.168.15.13
web IN A 192.168.15.10
db IN A 192.168.15.11
rp IN A 192.168.15.12
```


* Créer et configurer /var/named/15.168.192.db

```bash 
$ sudo nano 15.168.192.db

$TTL 604800
@ IN SOA ns1.tp2.cesi. root.tp2.cesi. (
                                                1001 ;Serial
                                                3H ;Refresh
                                                15M ;Retry
                                                1W ;Expire
                                                1D ;Minimum TTL
                                                )
;Name Server Information
@ IN NS ns1.tp2.cesi.

13.15.168.192 IN PTR ns1.tp2.cesi.

;PTR Record IP address to HostName
10 IN PTR web.tp2.cesi.
11 IN PTR db.tp2.cesi.
12 IN PTR rp.tp2.cesi.
```
* Accepter le trafic des ports 53/tcp et 53/udp
```bash
$ sudo firewall-cmd --add-port=53/tcp --permanent
$ sudo firewall-cmd --add-port=53/udp --permanent
$ sudo firewall-cmd --permanent --zone=trusted --add-source=192.168.15.10/32
$ sudo firewall-cmd --permanent --zone=trusted --add-source=192.168.15.11/32
$ sudo firewall-cmd --permanent --zone=trusted --add-source=192.168.15.12/32
$ sudo firewall-cmd --permanent --zone=trusted --add-port=53/tcp
$ sudo firewall-cmd --permanent --zone=trusted --add-port=53/udp
$ sudo firewall-cmd --reload
```

* Ajout du serveur DNS (ns1.tp2.cesi) sur les autres serveurs
```bash
$ sudo nano /etc/resolv.conf

nameserver 192.168.15.13
```
### 3. Test
```bash 
#Installation de bind-utils pour le test
$ sudo yum install bind-utils

# Résolution DNS de web.tp2.cesi (192.168.15.10) depuis db.tp2.cesi (192.168.15.13 étant le serveur DNS ns1.tp2.cesi)
$ dig web.tp2.cesi @192.168.15.13

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> web.tp2.cesi @192.168.15.13
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23721
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web.tp2.cesi.                  IN      A

;; ANSWER SECTION:
web.tp2.cesi.           604802  IN      A       192.168.15.10

;; AUTHORITY SECTION:
tp2.cesi.               604802  IN      NS      ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.           604802  IN      A       192.168.15.13

;; Query time: 0 msec
;; SERVER: 192.168.15.13#53(192.168.15.13)
;; WHEN: ven. déc. 18 14:08:46 CET 2020
;; MSG SIZE  rcvd: 91



# Résolution inverse
$ dig -x 192.168.15.10 @192.168.15.13

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> -x 192.168.15.10 @192.168.15.13
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59449
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;10.15.168.192.in-addr.arpa.    IN      PTR

;; ANSWER SECTION:
10.15.168.192.in-addr.arpa. 604800 IN   PTR     web.tp2.cesi.

;; AUTHORITY SECTION:
15.168.192.in-addr.arpa. 604800 IN      NS      ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.           604802  IN      A       192.168.15.13

;; Query time: 0 msec
;; SERVER: 192.168.15.13#53(192.168.15.13)
;; WHEN: ven. déc. 18 14:11:31 CET 2020
;; MSG SIZE  rcvd: 115
```

## II. Backup

* Installation de rsync : 
```bash
$ sudo yum install rsync
```
* Création du script /opt/backup.sh

```bash

```